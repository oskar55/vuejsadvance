import trae from 'trae'
import configService from './config'

const oscarMusicService = trae.create({
  baseUrl: configService.apiUrl
})

export default oscarMusicService
