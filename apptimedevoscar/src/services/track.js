import oscarMusicService from './oscar-music'

const trackService = {}

trackService.search = function (q) {
  const type = 'track'
  //es el objeto que toca el endpoint del api en su funcion searsh y carga dentro los parametros de busqueda
  //q es la consulta y type es lo que se busca en la API
  return oscarMusicService.get('/search', {
    params: { q: q, type: type }
  })
  //es como el success de ajax
  .then(function(res) {
    return res.data
  })
}

export default trackService
