import platziMusicService from './platzi-music'

const trackServices = {}

trackServices.search = function (q) {
  const type = 'track'

  platziMusicService.get('/search', {
    params: {q: q, type: type}
  })
    .then(res => res.data)
}

export default trackServices
